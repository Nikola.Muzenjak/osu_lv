import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV, cross_val_score

param_grid_SVM={'C':[1, 10, 100],
                'gamma':[10, 1, 0.1, 0.01]}

data=pd.read_csv('Vjezbanje/data.csv')

X=data[['Height','Weight']]
y=data['Gender']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

# skaliraj ulazne velicine
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

SVM_model=svm.SVC(kernel='rbf')
SVM_model.fit(X_train_n,y_train)
SVM_model_best=GridSearchCV(SVM_model, param_grid_SVM, cv=5, scoring='accuracy')

SVM_model_best.fit(X_train_n,y_train)
y_test_p=SVM_model_best.predict(X_test)
print(accuracy_score(y_test, y_test_p))