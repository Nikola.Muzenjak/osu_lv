import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler

# Ucitavanje csv file-a sa numpy bibliotekom
data=np.loadtxt('Vjezbanje/data.csv',delimiter=',',skiprows=1)

labels={1:'Male', 0:'Female'}

# Kako iz dataset-a izvuci odredene podatke
male=data[data[:,0] == 1,:]
print(male.shape)
print(data.shape)


X=data[:,1:]
y=data[:,0]

# Podjela na trening i testni skup te skaliranje podataka
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

sc=MinMaxScaler()
X_train_scale=sc.fit_transform(X_train)
X_test_scale=sc.fit_transform(X_test)




#Treniranje modela
KNN_model=KNeighborsClassifier(n_neighbors=5)
KNN_model.fit(X_train_scale,y_train)

y_pred=KNN_model.predict(X_test_scale)



disp= DecisionBoundaryDisplay.from_estimator(
    KNN_model,
    X_train_scale,
    response_method="predict",
    xlabel='x1',
    ylabel='x2',
    alpha=0.5,
)
# Prikaz KNN algoritma sa granicom odluke
scatter=disp.ax_.scatter(X_train_scale[:,0],X_train_scale[:,1],cmap='winter', c=y_train, edgecolors='k')

# Izrada matrice confuzije i njezin graficki prikaz
cm=confusion_matrix(y_true=y_test,y_pred=y_pred)

ConfusionMatrixDisplay(cm).plot()
print(f'točnost: {(cm[0,0]+cm[1,1])/(cm[0,0]+cm[1,1]+cm[0,1]+cm[1,0])*100}%')
print(f'preciznost: {cm[0,0]/(cm[0,0]+cm[1,0])*100}%')
print(f'odziv: {cm[0,0]/(cm[0,0]+cm[0,1])*100}%')

plt.show()