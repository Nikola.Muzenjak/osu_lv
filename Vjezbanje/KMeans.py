import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# Ucitavanje slike iz datoteke
image=Image.imread('Vjezbanje/test_1.jpg')

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = image.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))


print(image.shape)
print(img.shape)
print(img_array.shape)

# Treniranje modela 
    # n_init - broj koliko puta ce se izvesti ucenje sa trugim centrima
    # n_clusters - koliko ce grupa imati
    # init - Kako ce se generirati pocetni centeroidi
KMeans_model=KMeans(n_clusters=8, n_init=10, init='k-means++')
KMeans_model.fit(img_array)
labels=KMeans_model.predict(img_array)


# Dobivamo matricu centeroida
C=KMeans_model.cluster_centers_

print(labels)
new_img=np.empty_like(img_array)

# kreiranje slike koristeci samo centeroide grupiranih podataka
counter=0
for label in labels:
    new_img[counter,:]=C[label,:]
    counter=counter+1

# Transformiranje slike u 3D numpy polje koje je potrebno da bi se moglo iscrtati na ekran
new_img=new_img.reshape(img.shape)

plt.figure()
plt.imshow(new_img)
plt.show()
