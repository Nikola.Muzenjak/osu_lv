import pandas as pd
import matplotlib.pyplot as plt

data= pd.read_csv("data_C02_emission.csv")

data['Make']=data['Make'].astype('category')
data['Model']=data['Model'].astype('category')
data['Vehicle Class']=data['Vehicle Class'].astype('category')
data['Transmission']=data['Transmission'].astype('category')
data['Fuel Type']=data['Fuel Type'].astype('category')



plt.figure()
data['CO2 Emissions (g/km)'].plot(kind='hist')
plt.xlabel('CO2 Emissions (g/km)')


data.plot.scatter(x='Fuel Consumption City (L/100km)', y='CO2 Emissions (g/km)', c='Fuel Type', cmap='summer')



data.boxplot(column=['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')


plt.figure()
data.groupby('Fuel Type').size().plot(kind='bar')



plt.figure()
data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean().plot(kind='bar')

plt.show()