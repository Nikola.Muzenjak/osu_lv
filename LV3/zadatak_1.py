import pandas as pd

data= pd.read_csv("data_C02_emission.csv")


data['Make']=data['Make'].astype('category')
data['Model']=data['Model'].astype('category')
data['Vehicle Class']=data['Vehicle Class'].astype('category')
data['Transmission']=data['Transmission'].astype('category')
data['Fuel Type']=data['Fuel Type'].astype('category')

print(data.info())

print(f"Broj izostalih vrijednosti: {data.isnull().sum()}")
print(f"Broj dupliciranih vrijednosti: {data.duplicated().sum()}")

data.dropna(axis=0)
data.drop_duplicates()

data=data.reset_index(drop=True)

##      B

print(data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False).head(3)[['Make','Model','Fuel Consumption City (L/100km)']])
print("\n")
print(data.sort_values(by='Fuel Consumption City (L/100km)', ascending=True).head(3)[['Make','Model','Fuel Consumption City (L/100km)']])

##      C

x=data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(f"Number of veichles: {len(x)}")
print(f"Average CO2 emission: {x['CO2 Emissions (g/km)'].mean()}")
print("\n")

##      D

x=data[(data['Make'] == 'Audi')]
print(f"Number of Audi: {len(x)}")
x= x[(x['Cylinders'] == 4)]
print(f"Average CO2 emission: {x['CO2 Emissions (g/km)'].mean()}")
print("\n")

##      E

print(data.groupby('Cylinders')['Cylinders'].count())
print()
print(data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean())
print("\n")

##      F

print(f"Diesel average consumption: {data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].mean()}")
print(f"Regular average consumption: {data[data['Fuel Type'] == 'X']['Fuel Consumption City (L/100km)'].mean()}")
print("\n")

##      G
print(data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')].sort_values(by='Fuel Consumption City (L/100km)',ascending=False).head(1)[['Make','Model','Fuel Consumption City (L/100km)']])
print("\n")

##      H
print(f"Number of manual transmission vehicle: {len(data[data['Transmission'].str.startswith('M')])}")
print("\n")

##      I
numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
new_data=data.select_dtypes(include=numerics)
print(data.corr())