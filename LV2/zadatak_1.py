import numpy as np
import matplotlib.pyplot as pl


x=np.array([1, 2, 3, 3, 1])
y=np.array([1, 2, 2, 1, 1])
pl.plot(x,y,'r',linewidth=2, marker='.', markersize=7)
pl.axis([0,4,0,4])
pl.xlabel('X')
pl.ylabel('Y')
pl.title('Primjer')
pl.show()