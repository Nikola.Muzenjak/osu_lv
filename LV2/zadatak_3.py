import numpy as np
import matplotlib.pyplot as plt

img=plt.imread('road.jpg')

plt.figure()
plt.imshow( img, alpha=0.5)

rows, cols, pixels = img.shape
imgt=img[::,round(cols/4):round(cols/2)]
plt.figure()
plt.imshow(imgt)

imgr=np.rot90(img,axes=(1,0))
plt.figure()
plt.imshow(imgr)

imgf=np.flip(img,axis=1)
plt.figure()
plt.imshow(imgf)

plt.show()