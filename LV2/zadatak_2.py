import numpy as np
import matplotlib.pyplot as pl
import csv
def main():
    file=open('data.csv')
    csvreader=csv.reader(file)

    header = []
    header = next(csvreader)

    data=[]

    for row in csvreader:
        data.append(row)
    data=np.array(data)


    print(len(data))


    height=data[::, 1]
    weight=data[::, 2]
    pl.figure()
    pl.scatter(weight,height,s=0.1)
    pl.ylabel ('height')
    pl.xlabel ('weight')


    height=data[::50, 1]
    weight=data[::50, 2]
    pl.figure()
    pl.scatter(weight,height,s=0.1)
    pl.ylabel ('height')
    pl.xlabel ('weight')

    height=[float(x) for x in height]

    print(f"min height: {min(height)}")
    print(f"max height: {max(height)}")
    print(f"average height: {sum(height)/len(height)}")

    man = (data[:, 0] == 1)
    woman = (data[:, 0] == 0)
    
    height=data[man, 1]             #   This doesn't work
    height=[float(x) for x in height]

    print("Man")
    print(f"min height: {min(height)}")
    print(f"max height: {max(height)}")
    print(f"average height: {sum(height)/len(height)}")

    height=data[woman, 1]           #   This doesn't work
    height=[float(x) for x in height]

    print("Woman")
    print(f"min height: {min(height)}")
    print(f"max height: {max(height)}")
    print(f"average height: {sum(height)/len(height)}")

    pl.show()


if __name__=="__main__":
    main()