import numpy as np
import matplotlib.pyplot as plt

one= np.ones((50,50))
zero= np.zeros((50,50))
top_half=np.hstack((zero,one))
bottom_half=np.hstack((one,zero))
img=np.vstack((top_half,bottom_half))

plt.figure()
plt.imshow(img, cmap='gray')
plt.show()