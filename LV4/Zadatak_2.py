import pandas as pd
import matplotlib.pyplot as plt
import sklearn as skl
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn . preprocessing import OneHotEncoder
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
import math

data= pd.read_csv(r"C:\Users\JA\Desktop\programming\osu_lv\LV4\data_C02_emission.csv")
input= ['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Type']

ohe=OneHotEncoder()
data['Fuel Type']=ohe.fit_transform(data[['Fuel Type']]).toarray()

X= data[input].to_numpy()
y= data['CO2 Emissions (g/km)'].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

linearModel=lm.LinearRegression()
linearModel.fit(X_train,y_train)
y_predict= linearModel.predict(X_test)

for i in range(len(input)):
    plt.figure()
    plt.scatter(X_test[:,i], y_test, c='blue',s=0.5)
    plt.scatter(X_test[:,i], y_predict, c='red',s=0.5)
    plt.legend(('Real values','Predict values'))
    plt.xlabel(input[i])
    plt.ylabel('CO2 Emissions (g/km)')


MAE=mean_absolute_error(y_test,y_predict)
MSE= mean_squared_error(y_test,y_predict)
RMSE= math.sqrt(MSE)
MAPE= mean_absolute_percentage_error(y_test,y_predict)
R= r2_score(y_test,y_predict)

print(f'MSE= {MSE}\nRMSE= {RMSE}\nMAE= {MAE}\nMAPE= {MAPE}\nR^2= {R}\n\n')


plt.show()