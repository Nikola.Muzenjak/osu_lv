import pandas as pd
import matplotlib.pyplot as plt
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
import math

def print_metrics():
    MAE=mean_absolute_error(y_test,y_predict)
    MSE= mean_squared_error(y_test,y_predict)
    RMSE= math.sqrt(MSE)
    MAPE= mean_absolute_percentage_error(y_test,y_predict)
    R= r2_score(y_test,y_predict)

    print(f'MSE= {MSE}\nRMSE= {RMSE}\nMAE= {MAE}\nMAPE= {MAPE}\nR^2= {R}\n\n')



data= pd.read_csv(r"C:\Users\JA\Desktop\programming\osu_lv\LV4\data_C02_emission.csv")
input= ['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)']

print(data.info())

X= data[input].to_numpy()
y= data['CO2 Emissions (g/km)'].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)



plt.figure()
plt.scatter(X_train[::,2],y_train,c='blue',s=0.5)
plt.scatter(X_test[::,2],y_test,c='red',s=0.5)
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend(('Trenirani skup','Testni skup'))



plt.figure()
plt.hist(X_train[::,2], color='blue')

sc=MinMaxScaler()
X_train_scale=sc.fit_transform(X_train)
X_test_scale=sc.transform(X_test)

plt.hist(X_train_scale[::,2], color='red')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('Frequency')
plt.legend(('Before Scaling','After Scaling'))



linearModel= lm.LinearRegression()
linearModel.fit(X_train_scale, y_train)
print(f'{linearModel.intercept_} + {linearModel.coef_}')
y_predict= linearModel.predict(X_test_scale)

for i in range(5):
    plt.figure()
    plt.scatter(X_test_scale[:,i], y_test, c='blue',s=0.5)
    plt.scatter(X_test_scale[:,i], y_predict, c='red',s=0.5)
    plt.legend(('Real values','Predict values'))
    plt.xlabel(input[i])
    plt.ylabel('CO2 Emissions (g/km)')



for i in range(5):
    linearModel.fit(X_train_scale[:,0:i+1], y_train)
    y_predict= linearModel.predict(X_test_scale[:,0:i+1])
    print_metrics()

plt.show()


