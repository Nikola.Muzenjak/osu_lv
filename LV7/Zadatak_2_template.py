import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

images = ["LV7\\imgs\\test_1.jpg",
          "LV7\\imgs\\test_2.jpg",
          "LV7\\imgs\\test_3.jpg",
          "LV7\\imgs\\test_4.jpg",
          "LV7\\imgs\\test_5.jpg",
          "LV7\\imgs\\test_6.jpg"]
# ucitaj sliku
for i in range(1):
    img = Image.imread(images[i])

    # prikazi originalnu sliku
    plt.figure()
    plt.title("Originalna slika")
    plt.imshow(img)
    plt.tight_layout()

    # pretvori vrijednosti elemenata slike u raspon 0 do 1
    img = img.astype(np.float64) / 255

    # transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
    w, h, d = img.shape
    img_array = np.reshape(img, (w*h, d))

    # rezultatna slika
    img_array_aprox = img_array.copy()

    _, counts = np.unique(img_array_aprox, return_counts=True, axis=0)

    print(f"Postoji {len(counts)} različitih boja")

    km = KMeans(n_clusters=5, init="k-means++", n_init=10)
    km.fit(img_array_aprox)
    labels = km.predict(img_array_aprox)

    C = km.cluster_centers_

    counter = 0

    for i in labels:
        img_array_aprox[counter, :] = C[i, :]
        counter = counter+1

    img2 = img_array_aprox.reshape(img.shape)
    plt.figure()
    plt.title("Kvantizirana slika")
    plt.imshow(img2)
    plt.tight_layout()
    plt.show()


img = Image.imread(images[0])

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

inertia_list = []
k = [2,3,4,5,7,10]

for i in k:
    km = KMeans(n_clusters=i, init="k-means++", n_init=10)
    km.fit(img_array_aprox)
    inertia_list.append(km.inertia_)

plt.plot(k, inertia_list,'bx-')
plt.xlabel("Number of Clusters", size=13)
plt.ylabel("Inertia Value", size=13)
plt.show()


km= KMeans(n_clusters=5,init='k-means++',n_init=10)

img_array_aprox=img_array.copy()

km.fit(img_array_aprox)
labels=np.array(km.predict(img_array_aprox))
C = km.cluster_centers_

for i in set(labels):
    image=np.zeros_like(labels)
    image[labels==i]=1
    image=image.reshape(w,h)
    plt.imshow(image)
    plt.show()