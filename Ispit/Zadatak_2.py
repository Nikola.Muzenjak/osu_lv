import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler

data=np.loadtxt('Ispit/diabetes.csv',delimiter=',',skiprows=1)

X=data[:,0:-1]
y=data[:,-1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

### A
lm=LogisticRegression()
lm.fit(X_train,y_train)
###

### B
y_pred=lm.predict(X_test)
###

### C
cm=confusion_matrix(y_true=y_test,y_pred=y_pred)
ConfusionMatrixDisplay(cm).plot()
###

### D documentation = https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html
print(f'točnost: {(cm[0,0]+cm[1,1])/(cm[0,0]+cm[1,1]+cm[0,1]+cm[1,0])*100}%')
print(f'preciznost: {cm[1,1]/(cm[1,1]+cm[0,1])*100}%')
print(f'odziv: {cm[1,1]/(cm[1,1]+cm[1,0])*100}%')
###

plt.show()