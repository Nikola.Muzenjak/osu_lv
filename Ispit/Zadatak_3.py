import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from keras.models import load_model
from os.path import exists
import random

data=np.loadtxt('Ispit/diabetes.csv',delimiter=',',skiprows=1)

X=data[:,0:-1]
y=data[:,-1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

if not exists('Ispit/Neural Network'):
    ### A
    model= keras.Sequential()
    model.add(layers.Input(shape=(8,)))
    model.add(layers.Dense(12, activation='relu'))
    model.add(layers.Dense(8, activation='relu'))
    model.add(layers.Dense(1, activation='sigmoid'))

    model.summary()
    ###

    ### B
    model.compile(loss='categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])
    ###

    ### C
    epoch=20
    batch_size=10
    model.fit(X_train,
            y_train,
            batch_size=batch_size,
            epochs=epoch,
            validation_split=0.1)
    ###

    ### D
    model.save('Ispit/Neural Network')
    ###
else:
    model=load_model('Ispit/Neural Network')

### E
score = model.evaluate(X_test, y_test, verbose=0)
print(score)
###

### F
y_pred=model.predict(X_test)
cm=confusion_matrix(y_true=y_test,y_pred=y_pred)
ConfusionMatrixDisplay(cm).plot()
###

plt.show()