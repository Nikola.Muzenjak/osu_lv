import numpy as np
import matplotlib.pyplot as plt

### A
data=np.loadtxt('Ispit/diabetes.csv',delimiter=',',skiprows=1)

print(data.shape[0])
###

### B
data[~np.isnan(data[:,[5,7]]).any(axis=1),:] # odbacuje sve redove koji nemaju unesenu vrijednost
new_data=np.unique(data[:,[5,7]],axis=0) #vraca unique redove po BMI i godinama
print(new_data)
print(data.shape[0])
###

### C
plt.figure()
plt.scatter(data[:,5],data[:,7])
plt.xlabel('BMI')
plt.ylabel('Age')
plt.show()
###

### D
print('Max: ',np.max(data[:,5]))
print('Mean: ',np.mean(data[:,5]))
print('Min: ',np.min(data[:,5]))
###

### E
diabetes=data[data[:,8] == 1]
not_diabetes=data[data[:,8] == 0]
print('Has diabetes: ',len(diabetes))
print('Max: ',np.max(diabetes[:,5]))
print('Mean: ',np.mean(diabetes[:,5]))
print('Min: ',np.min(diabetes[:,5]))
print()
print('Dont have diabetes: ',len(not_diabetes))
print('Max: ',np.max(not_diabetes[:,5]))
print('Mean: ',np.mean(not_diabetes[:,5]))
print('Min: ',np.min(not_diabetes[:,5]))
###