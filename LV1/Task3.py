


def main():
    numbers=[]
    while True:
        try:
            number=input("Unesite broj: ")
            if number=="Done":
                break
            else:
                number=float(number)
        except ValueError:
            print("Not a number")
        else: 
            numbers.append(number)
    
    numberOfInputs= len(numbers)
    average= sum(numbers)/len(numbers)
    minimum=min(numbers)
    maximum=max(numbers)
    print(f"Number of inputs: {numberOfInputs}\nAverage: {average}\nMinimum: {minimum}\nMaximum: {maximum}")



if __name__ == "__main__":
    main()