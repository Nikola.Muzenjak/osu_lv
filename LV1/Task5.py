

def main():
    ham=[]
    spam=[]
    file=open('SMSSpamCollection.txt')
    for line in file:
        message=line.split('\t')
        text=message[1].replace('\n','')
        if message[0]=='ham':
            ham.append(text)
        elif message[0]=='spam':
            spam.append(text)

    print(f"Ham average words per message is: {AverageWordsForSpecificMessageType(ham)}")
    print(f"Spam average words per message is: {AverageWordsForSpecificMessageType(spam)}")
    print(f"Number of spam messages that end with exclamation point is: {CountMessagesThatEndWithExclamation(spam)} out of {len(spam)}")
    

def AverageWordsForSpecificMessageType(messages):
    sum=0

    for message in messages:
        words=message.split()
        sum=sum+len(words)

    return sum/len(messages)

def CountMessagesThatEndWithExclamation(messages):
    count=0
    for message in messages:
        if message.endswith('!'):
            count+=1
    return count


if __name__ == "__main__":
    main()