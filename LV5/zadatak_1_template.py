import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

plt.figure()
plt.scatter(X_train[:,0],X_train[:,1],cmap='winter', c=y_train)
plt.scatter(X_test[:,0],X_test[:,1], cmap='winter',c=y_test,marker='x')
plt.xlabel='x1'
plt.ylabel='x2'



logModel=LogisticRegression()
logModel.fit(X_train,y_train)
y_pred=logModel.predict(X_test)

disp= DecisionBoundaryDisplay.from_estimator(
    logModel,
    X_train,
    response_method="predict",
    xlabel='x1',
    ylabel='x2',
    alpha=0.5,
)

disp.ax_.scatter(X_train[:,0],X_train[:,1],cmap='winter', c=y_train, edgecolors='k')




matrix= ConfusionMatrixDisplay(confusion_matrix(y_test , y_pred ))
matrix.plot()

cm= confusion_matrix(y_test,y_pred)
print(f'točnost: {(cm[0,0]+cm[1,1])/(cm[0,0]+cm[1,1]+cm[0,1]+cm[1,0])*100}%')
print(f'preciznost: {cm[0,0]/(cm[0,0]+cm[1,0])*100}%')
print(f'odziv: {cm[0,0]/(cm[0,0]+cm[0,1])*100}%')



plt.figure()
for i in range(len(y_test)):
    if y_pred[i] == y_test[i]:
        color='green'
    else:
        color='black'

    plt.scatter(X_test[i,0],X_test[i,1],c=color)

plt.show()

