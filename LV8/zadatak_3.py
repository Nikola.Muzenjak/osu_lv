import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from keras.models import load_model
from skimage import color
from skimage import io
import random

#Ucitavanje slike
rgb_img=plt.imread('LV8/Test.png')
np.array(rgb_img)


# Image proccessing
grayscale_img=color.rgb2gray(rgb_img)
grayscale_img=grayscale_img.reshape(1,-1)


# Ucitavanje modela
model= load_model('ImageReader/')


# Rad sa slikom
prediction=model.predict(grayscale_img)


# Obrada izlaza u koristan podatak
prediction=(prediction >=0.5).astype(int)
prediction=np.argmax(prediction, axis=1)

print(f'Ovaj broj je: {prediction[0]}')